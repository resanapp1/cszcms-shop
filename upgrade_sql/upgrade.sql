/* For Version 1.0.4 */
/*
INSERT INTO `general_label` (`general_label_id`, `name`, `remark`, `lang_en`, `timestamp_update`) VALUES
('', 'shop_gst_txt', 'For shop gst text', 'GST(VAT)', '2017-01-13 10:53:09'),
('', 'shop_gst_show_txt', 'For gst show text', 'GST(VAT) %d%%', '2017-01-13 10:53:09'),
('', 'shop_promocode_txt', 'For promo code text', 'Promotional Code', '2017-01-13 10:53:09'),
('', 'shop_promo_show_txt', 'For promotional show text', 'Promotional code %d%% discount offer!', '2017-01-13 10:53:09'),
('', 'shop_promo_wrong', 'For promotional is wrong text', 'Your promotional code invalid!', '2017-01-13 10:53:09');
DROP TABLE IF EXISTS `shop_promocode`;
CREATE TABLE IF NOT EXISTS `shop_promocode` (
  `shop_promocode_id` int(11),
  `promocode` varchar(255),
  `remark` varchar(255),
  `start_date` date,
  `end_date` date,
  `discount` float,
  `active` int(11),
  `timestamp_create` datetime,
  `timestamp_update` datetime,
  PRIMARY KEY (`shop_promocode_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
ALTER TABLE `shop_config` DROP `paysbuy_active`, DROP `paysbuy_email`;
ALTER TABLE `shop_config` ADD `gst_vat` float NULL DEFAULT NULL AFTER `currency_code`;
ALTER TABLE `shop_config` ADD `exclude_gst_vat` INT NULL DEFAULT NULL AFTER `gst_vat`;
UPDATE `shop_config` SET `gst_vat` = '0' WHERE `shop_config`.`shop_config_id` = 1;
UPDATE `shop_config` SET `exclude_gst_vat` = '0' WHERE `shop_config`.`shop_config_id` = 1;
*/