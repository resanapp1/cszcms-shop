<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12">
        <!-- Start Products Admin Menu -->
        <?php echo $this->Shop_model->AdminMenu() ?>
        <!-- End Products Admin Menu -->
        <ol class="breadcrumb">
            <li class="active">
                <i><span class="glyphicon glyphicon-edit"></span></i> <?php echo $this->lang->line('shop_promo_code') ?>
            </li>
        </ol>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-12 col-md-12">
        <div class="h2 sub-header"><?php echo  $this->lang->line('shop_promo_code') ?>  <a role="button" href="<?php echo $this->Csz_model->base_link()?>/admin/plugin/shop/promoNew" class="btn btn-default btn-sm"><span class="glyphicon glyphicon-plus"></span> <?php echo  $this->lang->line('shop_promo_addnew') ?></a></div>
        <?php echo form_open($this->Csz_model->base_link() . '/admin/plugin/shop/promoEditSave/'.$this->uri->segment(5)); ?>               
        <div class="control-group">	
            <?php echo form_error('promocode', '<div class="alert alert-danger text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>'); ?>
            <label class="control-label" for="promocode"><?php echo $this->lang->line('shop_promo_text'); ?>*</label>
            <?php
            $data = array(
                'name' => 'promocode',
                'id' => 'promocode',
                'required' => 'required',
                'autofocus' => 'true',
                'class' => 'form-control',
                'maxlength' => '255',
                'value' => set_value('promocode', $promocode->promocode, FALSE)
            );
            echo form_input($data);
            ?>			
        </div> <!-- /control-group -->        
        <div class="control-group">
            <?php echo form_error('discount', '<div class="alert alert-danger text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>'); ?>
            <label class="control-label" for="discount"><?php echo $this->lang->line('shop_promo_discount'); ?>*</label>
            <div class="input-group">
                <?php
                $data = array(
                    'name' => 'discount',
                    'id' => 'discount',
                    'required' => 'required',
                    'autofocus' => 'true',
                    'class' => 'form-control keypress-number',
                    'maxlength' => '5',
                    'value' => set_value('discount', $promocode->discount, FALSE)
                );
                echo form_input($data);
                ?>
                <span class="input-group-addon"><b>%</b></span>
            </div>
        </div> <!-- /control-group -->
        <div class="control-group">	
            <?php echo form_error('start_date', '<div class="alert alert-danger text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>'); ?>
            <label class="control-label" for="start_date"><?php echo $this->lang->line('startdate_field'); ?>*</label>
            <?php
            $data = array(
                'name' => 'start_date',
                'id' => 'start_date',
                'required' => 'required',
                'autofocus' => 'true',
                'class' => 'form-control form-datepicker',
                'value' => set_value('start_date', $promocode->start_date, FALSE)
            );
            echo form_input($data);
            ?>
        </div> <!-- /control-group -->
        <div class="control-group">	
            <?php echo form_error('end_date', '<div class="alert alert-danger text-center" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>', '</div>'); ?>
            <label class="control-label" for="end_date"><?php echo $this->lang->line('enddate_field'); ?>*</label>
            <?php
            $data = array(
                'name' => 'end_date',
                'id' => 'end_date',
                'required' => 'required',
                'autofocus' => 'true',
                'class' => 'form-control form-datepicker',
                'value' => set_value('end_date', $promocode->end_date, FALSE)
            );
            echo form_input($data);
            ?>
        </div> <!-- /control-group -->
        <div class="control-group">
            <label class="control-label" for="remark"><?php echo $this->lang->line('remark_header'); ?></label>
            <?php
            $data = array(
                'name' => 'remark',
                'id' => 'remark',
                'class' => 'form-control',
                'maxlength' => '255',
                'value' => set_value('remark', $promocode->remark, FALSE)
            );
            echo form_input($data);
            ?>
        </div> <!-- /control-group -->
        <br>
        <div class="control-group">										
            <label class="form-control-static" for="active">
                <?php
                if ($promocode->active) {
                    $checked = 'checked';
                } else {
                    $checked = '';
                }
                $data = array(
                    'name' => 'active',
                    'id' => 'active',
                    'value' => '1',
                    'checked' => $checked
                );
                echo form_checkbox($data);
                ?> <?php echo $this->lang->line('shop_active'); ?></label>	
        </div> <!-- /control-group -->
        <br><br>
        <div class="form-actions">
            <?php
            $data = array(
                'name' => 'submit',
                'id' => 'submit',
                'class' => 'btn btn-lg btn-primary',
                'value' => $this->lang->line('btn_save'),
            );
            echo form_submit($data);
            ?> 
            <a class="btn btn-lg" href="<?php echo $this->csz_referrer->getIndex('shop'); ?>"><?php echo $this->lang->line('btn_cancel'); ?></a>
        </div> <!-- /form-actions -->
        <?php echo form_close(); ?>
        <!-- /widget-content --> 
    </div>
</div>